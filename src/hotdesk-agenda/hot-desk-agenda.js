import { LitElement, html, css } from 'lit-element';
import './hot-desk-lugares.js'
import '../hotdesk-alert/hot-desk-alert.js'

class HotDeskAgenda extends LitElement {
    static get properties() {
		return {
            name:{type:String},
            edificios: {type: Array},
            pisos: {type: Array},
            lugares: {type: Array},
            edificioDefault: {type: String},
            pisoDefault: {type: String},
            idConfirmacion: {type: String},
            internoDefault: {type: Boolean}
	    };
    }

    static get styles(){  
        return css`
            .card{
                display: inline-block;
                text-align:center;
                margin: 0px 0px 100px 0px;
            }

            .form-group{
                padding: 5px;
            }

            #confirmacion{
                position: absolute;
                z-index: 2;
                text-align: center;
                left:38%;
            }

            #formulario{
                display: inline-block;
                text-align: center;
                margin: 0px;
                position: relative;
                width: 100%;
            }

            #central{
                text-align: center;
                display: inline-block;
                padding: 15px;
                margin: 15px;
                z-index: 1;
            }

            h2{
                color: lightblue;
                padding: 10px;
            }

            label{
                color: gray;
                margin: 10px;
            }

        `;
    }

    constructor() {
        super();
        this.name = "";
        this.edificios = [];
        this.pisos= [];	
        this.lugares=[];
        this.edificioDefault = "";
        this.pisoDefault= 0;
        this.idConfirmacion = "";
        this.internoDefault = false;
        this.getEdificiosData();
    }
    
    render() {
        return html`	
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
            <hot-desk-alert class="d-none" id="confirmacion"></hot-desk-alert> 
            <div class="card text-center" id="formulario">
                <div id="central">
                    <h2>Agendar Desk</h2>
                    <div class="form-group justify-content-center">
                            <label>Nombre Completo</label>
                            <input type="text" @input="${this.updateName}" id="formName" class="form-control" placeholder="Nombre"/>
                            <label>Interno o Externo</label>
                            <select class="form-control" id="internoBoolean" @input="${this.updateInterno}">
                                <option value="">Elige una opción</option>
                                <option value="Interno">Interno</option>
                                <option value="Externo">Externo</option>
                            </select>
                    </div>
                    <div class="form-group justify-content-center">
                            <label>Edificios Disponibles</label>
                            <select name="Edificio" class="form-control" id="edificioSeleccionado" @input="${this.updatePiso}">
                                <option value="">Elige una opción</option>
                                ${this.edificios.map(
                                    function(edificio){ return html 
                                        `<option value="${edificio.nombre}">${edificio.nombre}
                                        </option>`;
                                    }.bind(this)
                                )}
                            </select>
                    </div>
                    <div class="form-group justify-content-center">
                            <label>Pisos Disponibles</label>
                            <select name="Piso" class="form-control" id="pisoSeleccionado" @input="${this.updateLugares}">
                                <option value="">Elige una opción</option>
                                ${this.pisos.map(
                                    function(piso){ return html 
                                        `<option value="${piso.piso}">${piso.piso}
                                        </option>`;
                                    }.bind(this)
                                )}
                            </select>
                    </div>
                    <div class="form-group justify-content-center">
                        <label>Lugares Disponibles</label>
                        <ul class="list-group-horizontal">
                            ${this.lugares.map(
                                function(lugar){ return html 
                                    `<hot-desk-lugares 
                                        id="${lugar.id}"
                                        edificio="${lugar.edificios}" 
                                        piso="${lugar.piso}"
                                        statusLugar="${lugar.statusLugar}" 
                                        nombre="${this.nombre}"  
                                        interno="${this.internoDefault}" 
                                        @agendar-lugar="${this.confirmLugar}" 
                                    </hot-desk-lugares>`;
                                }.bind(this)
                            )}
                        </ul>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-secondary" @click=${this.goBack}><strong>Regresar</strong></button>
                    </div>
                </div>
            </div>				
		`;
    }

    updateInterno(e){
        console.log("Update Interno o Externo");
        let combo = this.shadowRoot.getElementById("internoBoolean");
        let selected = combo.options[combo.selectedIndex].text;
        console.log(selected);
        if (selected == "Interno"){
            this.internoDefault = true;
        }
        else{
            this.internoDefault = false;
        }
    }

    updateLugares(e){
        console.log("Update Lugares");
        let combo = this.shadowRoot.getElementById("pisoSeleccionado");
        let selected = combo.options[combo.selectedIndex].text;
        console.log(selected);
        this.pisoDefault = selected;
        this.getLugaresData();
    }

    updatePiso(e){
        console.log("Update Pisos");
        let combo = this.shadowRoot.getElementById("edificioSeleccionado");
        let selected = combo.options[combo.selectedIndex].text;
        console.log(selected);
        this.edificioDefault = selected;
        this.getPisosData();
    }
    
    agendarLugar(e){
        console.log("agendarLugar en hot-desk-agenda");
        console.log("Se va a enviar PUT para agendar");	

        let xhr = new XMLHttpRequest();
    
        xhr.onload = function(){
          if (xhr.status == 200){
            console.log("Petición completada correctamente");
            console.log(xhr.responseText);
          }
        }.bind(this);

        var jsonObj = {
            nombre: this.name,
            interno: this.internoDefault,
            piso: this.pisoDefault,
            edificios: this.edificioDefault,
            statusLugar: "Disponible"
        }

        xhr.open("PUT","http://localhost:9999/escritorios/apartar/"+this.idConfirmacion);
        xhr.setRequestHeader("Access-Control-Allow-Origin","*");
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(jsonObj));
          
        console.log("Lugar Agendado");	
        
        this.showRegistro();
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.name = e.target.value;
    }

    getLugaresData(){
        console.log("Get lugares");
        console.log("Obteniendo datos de lugares");
    
        let xhr = new XMLHttpRequest();
        xhr.onload = function(){
          if (xhr.status == 200){
            console.log("Petición completada correctamente");
    
            let APIResponse = JSON.parse(xhr.responseText);
            
            this.lugares = APIResponse;
            alert(this.lugares);
          }
        }.bind(this);

        xhr.open("GET","http://localhost:9999/escritorios/apartar/");
        xhr.setRequestHeader("Access-Control-Allow-Origin","*");
        xhr.send();
    }

    getEdificiosData(){
        console.log("Get edificios");
        console.log("Obteniendo datos de edificios");
    
        let xhr = new XMLHttpRequest();
    
        xhr.onload = function(){
          if (xhr.status == 200){
            console.log("Petición completada correctamente");
    
            let APIResponse = JSON.parse(xhr.responseText);
            
            this.edificios = APIResponse.edificios;
            console.log(this.edificios);
          }
        }.bind(this);
    
        xhr.open("GET","http://localhost:8000/test-data/edificios.json");
        xhr.send();
    }

    getPisosData(){
        console.log("Get pisos");
        console.log("Obteniendo datos de pisos");
    
        let xhr = new XMLHttpRequest();
    
        xhr.onload = function(){
          if (xhr.status == 200){
            console.log("Petición completada correctamente");
    
            let APIResponse = JSON.parse(xhr.responseText);
            
            this.pisos = APIResponse.pisos;
            console.log(this.pisos);
          }
        }.bind(this);
    
        xhr.open("GET","http://localhost:8000/test-data/pisos.json");
        xhr.send();
    }

    confirmLugar(e){
        let dialog = this.shadowRoot.querySelector("hot-desk-alert");
        dialog.text = "¿Está seguro de agendar el lugar " + e.detail.lugar.id + "?";
        dialog.show();
        dialog.confirm = this.agendarLugar.bind(this,e);
        this.idConfirmacion = e.detail.lugar.id;
    }

    showRegistro(){
        let dialog = this.shadowRoot.querySelector("hot-desk-alert");
        dialog.text = "Su ID de registro es el: " + this.idConfirmacion;
        dialog.show();
        dialog.disClose();
        dialog.confirm = this.goBack.bind(this);
    }

    goBack(e) {
        console.log("goBack");	  
        e.preventDefault();	
        this.clearData();
        this.getEdificiosData();
        this.dispatchEvent(new CustomEvent("agenda-form-close",{}));
        
    }

    clearData(){
        this.name = "";
        this.edificios = [];
        this.pisos= [];	
        this.lugares=[];
        this.edificioDefault = "";
        this.pisoDefault= 0;
        this.idConfimacion = "";
        this.internoDefault = false;
    }

}

customElements.define('hot-desk-agenda', HotDeskAgenda);
