import { LitElement, html, css } from 'lit-element';

class HotDeskLugares extends LitElement {
    static get properties() {
		return {
            id:{type: String},
            nombre:{type:String},
            interno:{type: Boolean},
            piso:{type: Number},
            edificios:{type: String},
            statusLugar:{type: String},
            
            imgRec: {type: Object},
            habilitado: {type: String},
            deshabilitarBoton:{type:Boolean}
	    };
    }

    static get styles(){  
        return css`
            #disponibilidad{
                height: 20px;
                width: 20px;
                border:none !important;
            }

            #lugares
            {display:inline-block;
            }
        `;
    }

    constructor() {
        super();
        this.id = "";
        this.statusLugar = "";
        this.edificios = "";
        this.piso = 0;
        this.nombre = "";
        this.interno = false;

        this.imgRec = [];
        this.habilitado = "";
        this.deshabilitarBoton=false;
        
    }

    render() {
        this.validarOcupado();
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">		
            <li id="lugares" class="list-group-item">
                <h5>${this.id}</h5>
                <img src="${this.imgRec.src}" id="disponibilidad"></img>
                <button type="button" id="buton" class="btn btn-primary" @click="${this.agendarLugar}" disabled="${this.deshabilitarBoton}">AGENDAR</button>
            </li>				
		`;
    } 

    agendarLugar(e) {
        console.log("agendarLugar");
        e.preventDefault();
            
        console.log("Id: " + this.id);
        console.log("Estatus: " + this.statusLugar);
        console.log("Edificio: " + this.edificios);
        console.log("Piso: " + this.piso);
            
        this.dispatchEvent(new CustomEvent("agendar-lugar",{
            detail: {
                lugar:  {
                        id: this.id,
                        statusLugar: this.statusLugar,
                        edificios: this.edificios,
                        piso: this.piso,
                        nombre: this.nombre,
                        interno: this.interno
                    }
                }
            })
        );
    }

    validarOcupado(){
        console.log("Validando Lugar ocupado");
        if (this.statusLugar == "Disponible"){
            this.imgRec = {
                "src": "../img/disponible.png",
                "alt": "Disponible"
            };
            this.deshabilitarBoton=false;
        }else if(this.statusLugar == "Ocupado"){
            this.imgRec = {
                "src": "../img/nodisponible.png",
                "alt": "No Disponible"
            };
            this.deshabilitarBoton=true;
        }else{
            this.imgRec = {
                "src": "../img/inhabilitado.png",
                "alt": "Inhabilitado"
            };
            this.deshabilitarBoton=true;
        }
    }

    updated(changedProperties){
        console.log("updated");
        if(changedProperties.has("deshabilitarBoton")){
            if(this.deshabilitarBoton===true){
                this.shadowRoot.getElementById('buton').disabled=true;
            }else{
                this.shadowRoot.getElementById('buton').disabled=false;
            }
        }
    }

}

customElements.define('hot-desk-lugares', HotDeskLugares);
