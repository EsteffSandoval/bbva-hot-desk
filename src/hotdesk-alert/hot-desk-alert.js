import { LitElement, html, css } from 'lit-element';

class HotDeskAlert extends LitElement {

    static get properties() {
        return {
            title: {type: String},
            text: {type: String}
        };
    }

    static get styles(){  
        return css`
            #titulo{
                text-align: center;
                width: 300px;
                height: 200px;
                display: inline-block;
            }
            
        `;
    }

    constructor() {
        super();
        this.title = "";
        this.text = "";
        
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class="border rounded border-secondary bg-light" id="titulo">
                <div class="modal-header bg-dark text-white">
                    <center><h3>${this.title}</h3></center>
                </div>
                <div class="border m-2 p-2">
                    <form>
                        <div class="form-group">
                            <label>${this.text}</label>
                        </div>
                    </form>
                </div>
                <div>
                    <button id="can" type="submit" class="btn btn-secondary" @click=${this.close}><strong>Cancelar</strong></button>
                    <button id="conf" type="submit" class="btn btn-success" @click=${this.confirm}><strong>Confirmar</strong></button>
                </div>
            </div>
        `;
    }

    close(e){
        this.classList.add("d-none");
    }

    show(e){
        this.classList.remove("d-none");
    }

    disClose(e){
        this.shadowRoot.getElementById('can').disabled=true;
    }

    disConfirm(e){
        this.shadowRoot.getElementById('conf').disabled=true;
    }

    //overide
    confirm(e){}

}

customElements.define('hot-desk-alert', HotDeskAlert);
