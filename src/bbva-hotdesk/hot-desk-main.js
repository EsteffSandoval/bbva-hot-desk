import { LitElement, html, css } from 'lit-element';
import '../hotdesk-agenda/hot-desk-agenda.js'
import '../hotdesk-consulta/hot-desk-consulta.js'
import '../hotdesk-alert/hot-desk-alert.js'

class HotDeskMain extends LitElement {
	static get properties() {
		return {
			idConf: {type: String}
		};
	}

	static get styles() {
		return css`
			main{
				margin-top:10%; 
				margin-bottom:10%;
			}

			button {
				box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
				width: 50%;
				height:10%;
				margin: 20px 10px;
				font-size: 24px;
				font-weight: 900;
			}

			#agenda{
				display:inline-block;
				text-align: center;
				width: 100%;
				height: 100%;
				margin: 0px 0px 60px 0px;
			}
		`
	}


	constructor() {
		super();
		this.idConf = "";
	}


	render() {

		return html`	
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<div class="container-fluid">	
                <div id="inicio" class="px-4 py-5 my-5 text-center">	
                    <button type="button" @click=${this.showAgendar}  class="btn btn-primary btn-lg">AGENDAR</button>
					<br/>
                    <button type="button" @click=${this.showConsultar} class="btn btn-primary btn-lg">CONSULTAR</button>
				</div>

                <div>
                	<hot-desk-agenda id="agenda" class="d-none border rounded border-primary" 
                    	@agenda-form-close="${this.agendaFormClose}"
                	>
                	</hot-desk-agenda>
            	</div>

                <div>
                	<hot-desk-consulta id="consulta" class="d-none border rounded border-primary"
						@check-close="${this.consultaFormClose}"
						@check-out=${this.confirmOut} 
						
                	>
                	</hot-desk-consulta>
            	</div>
			</div>
		`;
	}

    showAgendar(){
        console.log("showAgendar");
        console.log("Mostrando Agendar lugar");
        
        this.shadowRoot.getElementById("agenda").classList.remove("d-none");
        this.shadowRoot.getElementById("inicio").classList.add("d-none");

    }

    showConsultar(){
        console.log("showConsultar");
        console.log("Mostrando Consultar lugar");
        
        this.shadowRoot.getElementById("consulta").classList.remove("d-none");
        this.shadowRoot.getElementById("inicio").classList.add("d-none");

    }

	agendaFormClose(){
        
        this.shadowRoot.getElementById("agenda").classList.add("d-none");
        this.shadowRoot.getElementById("inicio").classList.remove("d-none");

    }

    consultaFormClose(){
        
        this.shadowRoot.getElementById("consulta").classList.add("d-none");
        this.shadowRoot.getElementById("inicio").classList.remove("d-none");

    }

	confirmOut(e){
		console.log("confirmOut");
        let dialog = this.shadowRoot.querySelector('hot-desk-consulta').shadowRoot.querySelector("hot-desk-alert");
		dialog.text = "¿Estás seguro de que deseas desocupar el lugar?" ;
        dialog.show();
        dialog.confirm=this.doCheckOut.bind(this,e);
    }

	doCheckOut(e){
		console.log("doCheckOut en main");
		console.log("Se va a enviar POST para cambiar estatus a Inhabilitado");	

        let xhr = new XMLHttpRequest();
    
        xhr.onload = function(){
          if (xhr.status == 200){
            console.log("Petición completada correctamente");
    
            //let APIResponse = JSON.parse(xhr.responseText);
            //this.idConf = APIResponse.confirmacion;
            console.log(this.idConf);
          }
        }.bind(this);
    
        var jsonObj = {
            nombre: e.detail.name,
            interno: e.detail.internoDefault,
            piso: e.detail.pisoDefault,
            edificios: e.detail.edificioDefault,
            statusLugar: e.detail.statusLugar
        }

		console.log(JSON.stringify(jsonObj));
        xhr.open("PUT","http://localhost:9999/escritorios/apartar/"+ e.detail.id);
        xhr.setRequestHeader("Access-Control-Allow-Origin","*");
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(jsonObj));
          
        console.log("Estatus cambiado");	

		this.shadowRoot.querySelector('hot-desk-consulta').shadowRoot.querySelector("hot-desk-alert").close();
	}

}

customElements.define('hot-desk-main', HotDeskMain);
