import { LitElement, html, css } from 'lit-element';

class HotDeskHeader extends LitElement {
    static get properties() {
        return {
            title:{type: String}
        };
    }

    static get styles() {
		return css`
            #titulo{
                font: bold 30px Lucida Console, serif;
			}
		`
	}

    constructor() {
        super();
        this.title="eHotDesk"
    }

    render() {
        return html`
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <img src="https://cdn.worldvectorlogo.com/logos/bbva-2.svg" alt="" width="10%" height="10%" class="rounded float-start">
                <h1 class="navbar-brand text-right" id="titulo">
                    ${this.title}
                </h1>
            </div>
        </nav>
	`;
    }
}

customElements.define('hot-desk-header', HotDeskHeader);
