import { LitElement, html } from 'lit-element';
import './hot-desk-main.js';
import './hot-desk-header.js';
import './hot-desk-footer.js';

class HotDeskApp extends LitElement {
	static get properties() {
		return {	
		};
	}

	
	constructor() {
		super();		
	}		

	
	render() {
		return html`
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<div>
				<hot-desk-header></hot-desk-header>
			</div>
			<div><!--El m para que no se rompan los gutters el tema-->
				<hot-desk-main class="col-5"></hot-desk-main>
			</div>			
			<div>
				<hot-desk-footer class=" w-100 text-secondary text-center"></hot-desk-footer>
			</div>
		`;
  	}


}

customElements.define('hot-desk-app', HotDeskApp);
