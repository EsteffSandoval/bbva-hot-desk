import { LitElement, html, css } from 'lit-element';
import '../hotdesk-alert/hot-desk-alert.js'

class HotDeskConsulta extends LitElement {

    static get properties() {
		return {			
			idRegistro: {type: String},
            idConf: {type: String},
            peticion: {type: Object}
		};
	}
    
    static get styles(){  
        return css`
             .card{
                display: inline-block;
                text-align:center;
                margin: 0px 0px 100px 0px;
            }

            .form-group{
                padding: 5px;
            }

            #confirmacion{
                position: absolute;
                z-index: 2;
                text-align: center;
                left:38%;
            }

            #formulario{
                display: inline-block;
                text-align: center;
                margin: 0px;
                position: relative;
                width: 100%;
            }

            #checkList{
                text-align: center;
                display: inline-block;
                padding: 15px;
                margin: 15px;
                z-index: 1;
            }

            h3{
                color: lightblue;
            }

            label{
                color: gray;
                margin: 20px;
            }

            button{
                margin-top: 20px;
            }

        `;
    }

	constructor() {
		super();
			
		this.idRegistro = "";
        this.idConf = "";
        this.peticion = [];
		
    }
		
    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <hot-desk-alert class="d-none" id="confirmacion"></hot-desk-alert> 
            <div class="card text-center" id="formulario">
                <div id="checkList">
                    <h3>Check-in Check-out</h3>
                    <div class="form-group justify-content-center">
                        <label >ID Registro</label> 
                        <input type="text" class="form-control" @input="${this.updateId}" id="formId" placeholder="ID"/>
                    </div>
                    <div>
                        <button type="submit" disabled="${this.deshabilitarBoton}" class="btn btn-primary" @click=${this.doCheckIn}><strong>Check-in</strong></button>
                        <button type="submit" class="btn btn-danger" @click=${this.doCheckOut}><strong>Check-out</strong></button>
                        <button type="submit" class="btn btn-secondary" @click=${this.goBack}><strong>Regresar</strong></button>
                    </div>
                </div>
            </div>                
            
        `;
    }

    goBack(e){
        console.log("goBack");
        e.preventDefault();	
        //this.id=" "; 	
	    this.dispatchEvent(new CustomEvent("check-close",{}));
    }

    updateId(e){
        console.log("updateStatus");
        console.log("Actualizando la propiedad id con el valor " + e.target.value);
        this.idRegistro = e.target.value; 
        this.updateStatus();
    }

    doCheckIn(e){
        console.log("doCheckIn");
        this.dispatchEvent(new CustomEvent("check-in",{}));
    }
    
    doCheckOut(e){
        console.log("doCheckOut");
        //this.dispatchEvent(new CustomEvent("check-out",{}));
        this.dispatchEvent(new CustomEvent("check-out",{
            detail: {
                    id: this.peticion.id,
                    statusLugar: this.peticion.statusLugar,
                    edificios: this.peticion.edificios,
                    piso: this.peticion.piso,
                    nombre: this.peticion.nombre,
                    interno: this.peticion.interno
                
                }
            })
        );
    }

    updateStatus(e){
		console.log("updateStatus");
		console.log("Actualizando la propiedad estatus con el valor Ocupado");
        console.log("Se va a enviar POST para cambiar estatus");	

        let xhr = new XMLHttpRequest();
    
        xhr.onload = function(){
          if (xhr.status == 200){
            console.log("Petición completada correctamente");
    
            let APIResponse = JSON.parse(xhr.responseText);
            
            this.peticion = APIResponse;
            console.log(this.peticion);
          }
        }.bind(this);
    
        console.log(this.idRegistro)
        xhr.open("GET","http://localhost:9999/escritorios/apartar/"+this.idRegistro);
        xhr.setRequestHeader("Access-Control-Allow-Origin","*");
        xhr.send();
          
        console.log("Estatus cambiado");	
    }
        
}

customElements.define('hot-desk-consulta', HotDeskConsulta);
